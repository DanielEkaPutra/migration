<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <form action="/daftar" method="POST">
    @csrf
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <label>First name: </label><br><br>
        <input type="text" name="firstname"><br><br>
        <label>Last name: </label><br><br>
        <input type="text" name="lastname"><br><br>
        <label>Gender</label><br><br>
        <input type="radio" name="male"> Male <br>
        <input type="radio" name="female"> Female <br>
        <input type="radio" name="other"> Other <br><br>
        <label>Nationality: </label> <br><br>
        <select name="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singapuran">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select><br><br>
        <label>Language Spoken: </label><br><br>
        <input type="checkbox" name="indonesia">Bahasa Indonesian <br>
        <input type="checkbox" name="english">English <br>
        <input type="checkbox" name="other">Other <br><br>
        <label>Bio: </label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Submit">
    </form>
</body>
</html>